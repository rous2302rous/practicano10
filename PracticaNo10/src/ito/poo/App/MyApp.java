package ito.poo.App;
import ito.poo.clases.Cuenta;
import ito.poo.clases.Persistencia;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MyApp {
	
	public static Persistencia pe = new Persistencia();
	public static ArrayList<Cuenta> cuentas = new ArrayList<Cuenta>();
	public static Calendar calendario = new GregorianCalendar();
	public static int anio = calendario.get(Calendar.YEAR);
	public static int mes = calendario.get(Calendar.MONTH);
	public static int dia = calendario.get(Calendar.DAY_OF_MONTH);
	
	public static void run(){
		
		Cuenta c = new Cuenta(123, "Juan", LocalDate.of(2021, 11, 28), 200, LocalDate.of(2021, 11, 28));
		// tec = objeto de la clase Scanner
		Scanner tec = new Scanner(System.in);
		
		try {
			cuentas = Persistencia.leeDatos();
		}catch(Exception e) {
			System.err.println(e);
		}
				
		//op variable para guardar la opcion del usuario
		int op=0;
		do {
			op = 0;
			//menu
			System.out.println("Escoga una opcion\n 1)Agregar cuenta\n 2)Listar cuentas\n 3)hacer retiro"
					+ " de una cuenta\n 4)hacer deposito en una cuenta\n 5)Salir");
			op = tec.nextInt();
			
			switch(op) {
			// agregar cuenta de usuario
			case 1:
				boolean band=true;
				System.out.println("Inserta tu numero de cuenta:");
				int numero = tec.nextInt();
				for(int i=0; i<cuentas.size(); i++) {
					if(numero==cuentas.get(i).getNumeroCuenta()) {
						band=false;
					}
				}
				if(band) {
					System.out.println("Inserta tu nombre:");
					String nombre = tec.next();
					cuentas.add(new Cuenta(numero, nombre, LocalDate.of(anio, mes, dia), 0, LocalDate.of(anio, mes, dia)));
				}else {
					System.out.println("Este numero de cuenta ya existe");
				}
				break;
			//Mostrar cuentas de usuarios
			case 2:
				System.out.println(cuentas);
				break;
			//Hacer retiro
			case 3:
				int a=0, i=0;
				System.out.println("Ingresa el numero de tu cuenta");
				a = tec.nextInt();
				System.out.println("Ingresa la cantidad de dinero que quieres retirar");
				i = tec.nextInt();
				if(c.retiro(i, a)) {
					System.out.println("Su retiro se ha completado");
				}else {
					System.out.println("Su retiro no se ha podido completar");
				}
				break;
			//Hacer deposito
			case 4:
				int b=0, j=0;
				System.out.println("Ingresa el numero de tu cuenta");
				b = tec.nextInt();
				System.out.println("Ingresa la cantidad de dinero que quieres depositar");
				j = tec.nextInt();
				c.deposito(j, b);
				break;
			//Salir
			case 5:
				try {
					Persistencia.grabaDatos(cuentas);
				}catch(Exception e){
					System.err.println(e.getMessage());
				}
				System.out.println("Adios.");
				break;
			default:
				System.out.println("Esta opcion no existe...");
						
			}
		}while(op!=5);
		
	}
	
	public static void main(String[] args){
		// TODO Auto-generated method stub
		run();
	}

}
