package ito.poo.clases;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

import ito.poo.App.MyApp;

public class Cuenta implements Comparable<Cuenta>, Serializable{

	private int NumeroCuenta;
	private String nombrecliente; 
	private LocalDate fechadeapertura; 
	private int saldo;
	private LocalDate últimaactualizacion;
	
	public static Calendar calendario = new GregorianCalendar();
	public static int anio = calendario.get(Calendar.YEAR);
	public static int mes = calendario.get(Calendar.MONTH);
	public static int dia = calendario.get(Calendar.DAY_OF_MONTH);
	
	public Cuenta(int numeroCuenta,
			String nombrecliente,
			LocalDate fechadeapertura,
			int saldo,
			LocalDate Ultimaactualizacion) {
		this.NumeroCuenta = numeroCuenta;
		this.nombrecliente = nombrecliente;
		this.fechadeapertura = fechadeapertura;
		this.saldo = saldo;
		this.últimaactualizacion = Ultimaactualizacion;
	}
	
	public boolean retiro(int valor, int numero) {
		MyApp my = new MyApp();
		int contador=0, posicion=0, result=0;
		
		for(contador=0; contador<my.cuentas.size(); contador++) {
			if(numero==my.cuentas.get(contador).getNumeroCuenta()) {
				posicion = contador;
				contador = my.cuentas.size();
				result =1;
			}
		}
		if(result==0) {
			
			System.out.println("Su numero de cuenta no existe");
			return false;
			
		}
		if(valor<=my.cuentas.get(posicion).getSaldo()) {
			
			result = my.cuentas.get(posicion).getSaldo() - valor;
			my.cuentas.set(posicion, (new Cuenta(my.cuentas.get(posicion).getNumeroCuenta(), my.cuentas.get(posicion).getNombrecliente(), my.cuentas.get(posicion).getFechadeapertura(), result, LocalDate.of(anio, mes, dia))));
			return true;
			
		}else {
			
			System.out.println("No tiene suficiente dinero");
			return false;
			
		}
		
	}
	
	public void deposito(int valor, int numero) {
		MyApp my = new MyApp();
		int contador=0, posicion=0, result=0;
		
		for(contador=0; contador<my.cuentas.size(); contador++) {
			if(numero==my.cuentas.get(contador).getNumeroCuenta()) {
				posicion = contador;
				contador = my.cuentas.size();
				result =1;
			}
		}
		if(result==0) {
			
			System.out.println("Su numero de cuenta no existe");
			
		}else {
			
			my.cuentas.set(posicion, (new Cuenta(my.cuentas.get(posicion).getNumeroCuenta(), my.cuentas.get(posicion).getNombrecliente(), my.cuentas.get(posicion).getFechadeapertura(), my.cuentas.get(posicion).getSaldo()+valor, LocalDate.of(anio, mes, dia))));
			System.out.println("Su saldo ha sido actualizado a: "+my.cuentas.get(posicion).getSaldo());
			
		}
		
	}

	public int getNumeroCuenta() {
		return NumeroCuenta;
	}

	public void setNumeroCuenta(int numeroCuenta) {
		NumeroCuenta = numeroCuenta;
	}

	public String getNombrecliente() {
		return nombrecliente;
	}

	public void setNombrecliente(String nombrecliente) {
		this.nombrecliente = nombrecliente;
	}

	public LocalDate getFechadeapertura() {
		return fechadeapertura;
	}

	public void setFechadeapertura(LocalDate fechadeapertura) {
		this.fechadeapertura = fechadeapertura;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public LocalDate getÚltimaactualización() {
		return últimaactualizacion;
	}

	public void setÚltimaactualización(LocalDate últimaactualización) {
		this.últimaactualizacion = últimaactualización;
	}

	@Override
	public String toString() {
		return "Cuenta [numero de cuenta =" + NumeroCuenta + ", nombre=" + nombrecliente + ", saldo=" + saldo + ", fecha de apertura=" + fechadeapertura
				+ ", ultima actualización =" +últimaactualizacion + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + NumeroCuenta;
		result = prime * result + ((fechadeapertura == null) ? 0 : fechadeapertura.hashCode());
		result = prime * result + ((nombrecliente == null) ? 0 : nombrecliente.hashCode());
		result = prime * result + saldo;
		result = prime * result + ((últimaactualizacion == null) ? 0 : últimaactualizacion.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cuenta other = (Cuenta) obj;
		if (NumeroCuenta != other.NumeroCuenta)
			return false;
		if (fechadeapertura == null) {
			if (other.fechadeapertura != null)
				return false;
		} else if (!fechadeapertura.equals(other.fechadeapertura))
			return false;
		if (nombrecliente == null) {
			if (other.nombrecliente != null)
				return false;
		} else if (!nombrecliente.equals(other.nombrecliente))
			return false;
		if (saldo != other.saldo)
			return false;
		if (últimaactualizacion == null) {
			if (other.últimaactualizacion != null)
				return false;
		} else if (!últimaactualizacion.equals(other.últimaactualizacion))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Cuenta o) {
		// TODO Auto-generated method stub
		if(this.NumeroCuenta!=o.getNumeroCuenta())
			return this.NumeroCuenta-o.getNumeroCuenta();
		else if(this.nombrecliente.compareTo(o.getNombrecliente())!=0)
			return this.nombrecliente.compareTo(o.getNombrecliente());
		else if(this.fechadeapertura.compareTo(o.getFechadeapertura())!=0)
			return this.fechadeapertura.compareTo(o.getFechadeapertura());
		else if(this.últimaactualizacion.compareTo(o.getÚltimaactualización())!=0)
			return this.últimaactualizacion.compareTo(o.getÚltimaactualización());
		return this.saldo-o.getSaldo();
	}

	
	
}
