package ito.poo.clases;

import java.util.ArrayList;

import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Persistencia {
	
	public static void grabaDatos(ArrayList<Cuenta> l) throws FileNotFoundException, IOException {
		ObjectOutputStream file;
		file = new ObjectOutputStream(new FileOutputStream("datos.dat"));
		for(Cuenta c: l) 
			file.writeObject(c);
		file.close();
		
	}
	
	public static ArrayList<Cuenta> leeDatos() throws ClassNotFoundException, IOException{
		ArrayList<Cuenta> l = new ArrayList<Cuenta>();
		ObjectInputStream file;
		file= new ObjectInputStream(new FileInputStream("datos.dat"));
		Cuenta p;
		try {
		    while((p=(Cuenta)file.readObject())!=null) {
			    l.add(p);
		     }
		}catch(Exception e) {
			
		}
		file.close();
		return l;
	}
	
	

}
